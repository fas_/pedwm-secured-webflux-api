# JWT Autentication in Spring Webflux

This example creates a controller AuthRestAPI Controller to authenticate and register new clients that are able to use Notes API.

## Structure of the project

The structure of this project is as follows:
- controllers - with REST endpoints to be accessed by clients
- repositories - interfaces for reactive repositories
- security - configuration of security, jwt token generation 

## Security

The security is based on the ``AppUserDetails`` model, which models users on our application.

Security configuration is made in the file ``AppWebFluxSecurityConfig`` where we define how our routes and endpoints should be protected. In this example only the routes ``/login`` and ``/register`` can be accessed without an authentication token.

``JWTUtil`` is an help class which generates a bean to create and perform token related operations such as encoding user id and roles in the token and decode user and roles from the token.

``USerAuthService`` and ``SecurityContext `` are classes used to confirm that the token is authenticated and create a security context based on the received token. It will help define which methods the token can access based on user roles.

Finally, each route in the ``NotesController`` is annotated with ``@PreAuthorize("hasRole('ADMIN')")`` which details the level of user role that can call that endpoint successfully.

##  Final considerations

When using this API it is required to call ``/login`` to obtain a valid token, and then use the token on each requests to the Notes API.

The token should be included in a http header with the format:
- ``Authorization: Bearer (token)``

You can also test this API using postman. First we have to obtain a token from the ``login``
endpoint:
![alt text](./src/main/resources/static/postman_1.png "Login with postman")

Via XMLHttpRequest the code would seem link this:

``` javascript

function checkLogin(e){
    e.preventDefault();

    var data = {};
    data.username = document.getElementById(...).value;
    data.password  = document.getElementById(...).value;

    var json = JSON.stringify(data);
    var url  = "http://localhost:8080/login";
    var xhr  = new XMLHttpRequest();
    xhr.open('POST', url, true)
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = function () {
        console.log(xhr.responseText);

        var token = JSON.parse(xhr.responseText).token;
        if (xhr.readyState == 4 && xhr.status == "200") {
            // save token in browser's local storage
            window.localStorage.setItem('token',token);
            // change to user view
            // ToDo
        } else {
            alert('error in request');
        }
    }
    xhr.send(json);
}
```
 

Then we can use the token to interact with our Notes API using the Authorization http header
![alt text](./src/main/resources/static/postman_2.png "Authorized endpoint with postman")

Via XMLHttpRequest to get all notes:

``` javascript

function getNotes(){
    var url  = "http://localhost:8080/notes";
    var xhr  = new XMLHttpRequest();
    xhr.open('GET', url, true);

    // get autorization token from local storage
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem('token'));
    xhr.onload = function () {
        console.log(xhr.responseText);

        var notes = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            // ToDo handle response
        } else {
            alert('error in request');
        }
    }
    xhr.send(null);
}

```