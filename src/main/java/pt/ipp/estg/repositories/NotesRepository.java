package pt.ipp.estg.repositories;

import pt.ipp.estg.models.Note;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotesRepository extends ReactiveMongoRepository<Note, String> {
}
