package pt.ipp.estg.repositories;

import pt.ipp.estg.models.AppUserDetails;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveMongoRepository<AppUserDetails, String> {

    Mono<AppUserDetails> findFirstByUsername(Mono<String> username);
}
