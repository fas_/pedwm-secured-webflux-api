package pt.ipp.estg.controllers;

import org.springframework.web.bind.annotation.RequestBody;
import pt.ipp.estg.models.Note;
import pt.ipp.estg.repositories.NotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class NoteController {

    @Autowired
    private NotesRepository notesRepository;

    @GetMapping("/notes")
    @PreAuthorize("hasRole('USER')")
    Flux<Note> getNotes(){
        return this.notesRepository.findAll();
    }

    @PostMapping("/note")
    @PreAuthorize("hasRole('USER')")
    Mono<Note> postNotes(@RequestBody Note note){
        return this.notesRepository.save(note);
    }
}
