package pt.ipp.estg.controllers;

import pt.ipp.estg.models.TokenDTO;
import pt.ipp.estg.repositories.UserRepository;
import pt.ipp.estg.models.AppUserDetails;
import pt.ipp.estg.models.Role;
import pt.ipp.estg.models.UserDTO;
import pt.ipp.estg.security.JWTutil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AuthRestAPI {

    @Autowired
    private JWTutil jwtUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @PostMapping("/login")
    public Mono<ResponseEntity<?>> login(@RequestBody UserDTO user) {

        AppUserDetails u = new AppUserDetails();
        u.setUsername(user.getUsername());
        u.setPassword(user.getPassword());

        return this.userRepository.findFirstByUsername(Mono.just(user.getUsername())).map((user_saved) -> {
            if (this.passwordEncoder.matches(u.getPassword(),user_saved.getPassword())) {
                return ResponseEntity.ok(new TokenDTO(jwtUtil.generateToken(user_saved)));
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }).defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @PostMapping("/register")
    public Mono<ResponseEntity<UserDTO>> register(@RequestBody UserDTO user) {
        List<Role> user_roles = new ArrayList<Role>();
        user_roles.add(Role.USER);
        AppUserDetails u = new AppUserDetails();
        u.setUsername(user.getUsername());
        u.setPassword(this.passwordEncoder.encode(user.getPassword()));
        u.setRoles(user_roles);

        return this.userRepository.save((u)).map((userSaved) -> {
                UserDTO uDTO = new UserDTO();
                uDTO.setUsername(userSaved.getUsername());
                uDTO.setPassword(userSaved.getPassword());
                return ResponseEntity.ok(uDTO);
        }).defaultIfEmpty(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

}
