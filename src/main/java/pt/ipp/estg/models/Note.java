package pt.ipp.estg.models;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Note {


    String title;
    String description;

    public Note() {
        this.title = "";
        this.description = "";
    }

    public Note(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
